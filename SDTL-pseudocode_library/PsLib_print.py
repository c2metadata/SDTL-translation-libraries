import pandas as pd
import io
import os
import json
import csv
import tkinter as tk
from tkinter import filedialog

##*************************************************
##   This program reads the Pseudocode library
##   and ouputs the template for each SDTL
##   element.
##   File created is PseudocodeLibraryTexts.txt
##*************************************************


## -- Prompt for Function Library file ---
root = tk.Tk()
root.withdraw()
file_path = filedialog.askopenfilename()
print(file_path)
print("............")

## -- write filepath to file --
with open('PseudocodeLibraryTexts.txt', 'w', newline='') as txtfile:
    txtfile.write(file_path )
    txtfile.write("\n.....................\n")



## -- Read Function Library file ---
with open(file_path) as f:
      pslib = json.load(f)


sdtl_cnt = len(pslib["PseudocodeLibrary"])
param_cnt = len(pslib["PseudocodeLibrary"][0]["paramters"])
ps_str = ""
sep_str = "\n__________________________\n"

## -- iterate over SDTL types --

## -- iterate over functions -- 
for x in range(sdtl_cnt) :

    if pslib["PseudocodeLibrary"][x]["BaseText"] is None:
        ps_str = None
    else:
        ps_str = pslib["PseudocodeLibrary"][x]["SDTLname"] + " : \n" +  pslib["PseudocodeLibrary"][x]["BaseText"]
         
    ## -- iterate over multiple functions in same language --
    for y in range(param_cnt):

        param_text =  pslib["PseudocodeLibrary"][x]["paramters"][y]["text"]
        if param_text is not None : ps_str = ps_str + param_text

    if ps_str is not None :
        print(ps_str)
        print("__________________________")

        ## -- write elements to text file --
        with open('PseudocodeLibraryTexts.txt', 'a', newline='') as txtfile:
            txtfile.write(ps_str )
            txtfile.write(sep_str)
