import pandas as pd
import io
import os
import json
import csv
import tkinter as tk
from tkinter import filedialog

##*************************************************
##  This program reads the Function Library and
##  outputs a list of functions with their pseudocode
##  templates.
##  File created is FunctionLibrary_Pseudo.txt
##*************************************************

## -- Prompt for Function Library file ---
root = tk.Tk()
root.withdraw()
file_path = filedialog.askopenfilename()
print(file_path)

## -- Read Function Library file ---
with open(file_path) as f:
      fl3 = json.load(f)

## -- send counts by function type to terminal --
print("****  number of functions by type *******")
print('horizontal',  len(fl3['horizontal']))
print('vertical', len(fl3['vertical']))
print('collapse', len(fl3['collapse']))
print('logical', len(fl3['logical']))


## -- write file path to file --
with open('FunctionLibrary_Pseudo.txt', 'w', newline='') as txtfile:
    txtfile.write(file_path )
    txtfile.write("\n...............................\n" )


fun_types = ['horizontal', 'vertical','collapse', 'logical']
## -- iterate over function types --
for y in fun_types :
    print("******  ", y, "   *******")

    txtline = "\n\n******  " + y + "   *******"
    with open('FunctionLibrary_Pseudo.txt', 'a', newline='') as txtfile:
        txtfile.write(txtline)

## -- iterate over functions -- 
    for x in range(len(fl3[y])) :
        print(fl3[y][x]['SDTLname'], "--->|", fl3[y][x]['Pseudocode'], "|")

        ## -- write elements to text file --
        txtline = "\n" + fl3[y][x]['SDTLname'] + "--->|" + fl3[y][x]['Pseudocode'] + "|"
        with open('FunctionLibrary_Pseudo.txt', 'a', newline='') as txtfile:
            txtfile.write(txtline)

	
