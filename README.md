This repository contains translation files that describe:   
1. converting functions from SPSS, Stata, SAS, and R into SDTL  
2. converting SDTL into pseudocode (natural language)   