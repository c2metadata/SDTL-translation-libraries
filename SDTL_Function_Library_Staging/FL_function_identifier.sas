data statfcns;
   input xvar;
   datalines;
1
2
3
;
run;

data s1;
   set statfcns;

pdf_gamma = PDF('GAMMA', .25,1.5,2);
rand_gamma = RAND('GAMMA', 1.5, 2);
cdf_gamma = CDF('GAMMA', .25,1.5,2);
quant_gamma = QUANTILE('GAMMA', .25,1.5,2);

pdf_LOGNORMAL = PDF('LOGNORMAL', .25, 1.5, 2);
rand_LOGNORMAL = RAND('LOGNORMAL',  1.5, 2);
cdf_LOGNORMAL = CDF('LOGNORMAL', .25,  1.5, 2);
quant_LOGNORMAL = QUANTILE('LOGNORMAL', .25,  1.5, 2);

RUN;
