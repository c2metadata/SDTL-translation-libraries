use "C:\Users\altergc\Documents\ICPSR\Project development\metadata_capture\Functions\Function_Library_1-1\Stata_egen_test.dta", clear

sort grp
by grp: egen gmode1 =mode(quant)
by grp: egen gmodeMin =mode(quant), minmode
by grp: egen gmodeMax =mode(quant), maxmode
egen modeNum1 =mode(quant), nummode(1)
egen modeNum2 =mode(quant), nummode(2)

by grp: egen rank1= rank(quant)
by grp: egen rankField= rank(quant), field
by grp: egen rankTrack= rank(quant), track
by grp: egen rankUniq= rank(quant), unique


gener normPdf = normalden(quant/10, -1.5, 2)
gener normalRand = rnormal(-1.5,2)
gener  normaCDF =normal(quant/10)
gener  normInv =invnormal(quant/10)

