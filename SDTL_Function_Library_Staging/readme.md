This folder has files for testing three features proposed for the Function Library:    
1. “Stata_egen” as a Boolean property   
2. Using “|” in parameter names for Stata `egen` functions   
3. “function_identifier” to be used when a parameter in a function must be used to identify the appropriate SDTL function   
   
Explanations are below.

Files in this folder:   
   
"SDTL_Function_Library_1-1-1.json" -- test version of the Function Library with the proposed features

"Stata_egen_test.do"  -- Stata script for testing new egen features   
"Stata_egen_test.xml" -- DDI file describing the data used in the test   
"Stata_egen_test.dta" -- Data file used in the test.   
   
"FL_function_identifier.sas" -- SAS script for testing “function_identifier”   
"statfcns_DDI.xml"  -- DDI file used in the SAS test   
"statfcns.sas7bdat" -- Data file used in the SAS test   
   
   
A. Add “Stata_egen” as a Boolean property.   
Description:
This property will identify functions that are used in the Stata `egen` command.    

The Stata `egen` command is a specialized form of the `generate` (SDTL `Compute`) command to create new variables.  Functions used in `egen` commands are very heterogeneous, including string manipulation functions, calculations involving a list of variables on each row (SDTL horizontal functions), and functions that use values from multiple rows (SDTL Vertical functions).   The format of functions used in `egen` commands also differs from other functions.  Most functions in `egen` commands have only one parameter inside the parentheses following the function name, which may be a list of variables (SDTL VariableListExpression) or values (SDTL ValueListExpression).  Additional parameters to the function are passed as Stata “options” following a comma in the command, for example
```
egen cut(Xvar),{at( 5, 10, 15, 20) icodes label
```
B.  Using “|” in parameter names for Stata `egen` functions with lists of alternative keywords
Example: `” head|last|tail”`
Parameters for functions in Stata `egen` commands are occasionally given as a list of alternative keywords.  For example, the Stata manual provides this syntax:
```
ends(strvar)[, punct(pchars) trim [head|last|tail]]
```
In this case, “head”, “last”, and “tail” are actually alternative values for a single parameter, because only one can be selected, and Stata uses “head” as the default if no option is given.  This In the SDTL Function Library, the “name” property for this parameter is given as `” head|last|tail”` to indicate that these three keywords belong to only one parameter.    

C. “function_identifier” to be used when a parameter in a function must be used to identify the appropriate SDTL function  

This “function_identifier” property will be used when a function parameter in the source language is needed to identify the corresponding function in SDTL.  When the “function_identifier” is TRUE, the Parser should use this parameter from the source language to find the appropriate SDTL function.   

An example of the use of this property is the implementation of functions for statistical distributions in SAS.  Several functions are associated with every statistical distribution to describe different aspects of the distribution, such as a probability density function (PDF), a cumulative distribution function (CDF), and a quantile function.  SDTL follows the convention used in most statistical languages by putting both the name and the aspect of the distribution in the name of the function, such as gamma_pdf(x, y, z) and normal_cdf().  In SAS, however, functions are arranged by aspect, and the name of the distribution is a parameter of the function, such as PDF(‘GAMMA’, x, y, z) and CDF(‘NORMAL’, x, y, z).  Thus, the first parameter in the SAS function is needed to identify the corresponding SDTL function. 
Note: Although the model used by SAS for functions describing statistical distributions appears more succinct, it has an important disadvantage.  Statistical distributions have different numbers and types of parameters.  For example, gamma distributions are identified by three parameters (`shape`, ‘scale’, and ‘location’), but the F distribution has only two parameters (`numerator degrees of freedom` and `denominator degrees of freedom`).
