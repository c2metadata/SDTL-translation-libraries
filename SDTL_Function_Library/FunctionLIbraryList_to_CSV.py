import pandas as pd
import io
import os
import json
import csv
import tkinter as tk
from tkinter import filedialog

## -- Prompt for Function Library file ---
root = tk.Tk()
root.withdraw()
file_path = filedialog.askopenfilename()
print(file_path)

## -- Read Function Library file ---
with open(file_path) as f:
      fl3 = json.load(f)

## -- send counts by function type to terminal --
print("****  number of functions by type *******")
print('horizontal',  len(fl3['horizontal']))
print('vertical', len(fl3['vertical']))
print('collapse', len(fl3['collapse']))
print('logical', len(fl3['logical']))


## -- write column headers to CSV file --
with open('FunctionLibraryList.csv', 'w', newline='') as csvfile:
            outwrite = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
            outwrite.writerow(['Type', 'SDTL', 'Definition', 'SPSS', 'SAS', 'Stata', 'R', 'Python' ] )



fun_types = ['horizontal', 'vertical','collapse', 'logical']
## -- iterate over function types --
for y in fun_types :
    print("******  ", y, "   *******")
    print( "SDTLname                        SPSS       SAS        Stata      R         Python")

## -- iterate over functions -- 
    for x in range(len(fl3[y])) :

        ## -- iterate over multiple functions in same language --
        for z in [0, 1, 2]:

            ## -- check for at least one source language in this loop ---
            cntLangs=0
            
            fnSDTL = fl3[y][x]['SDTLname']
            fnDefinition = fl3[y][x]['definition']
            if len(fl3[y][x]['SPSS']) >= z + 1:
                fnSPSS = fl3[y][x]['SPSS'][z]['function_name']
                cntLangs = cntLangs + 1
            if len(fl3[y][x]['SAS']) >= z + 1:
                fnSAS = fl3[y][x]['SAS'][z]['function_name']
                cntLangs = cntLangs + 1
            if len(fl3[y][x]['Stata']) >= z + 1:
                fnStata = fl3[y][x]['Stata'][z]['function_name']
                cntLangs = cntLangs + 1 
            if len(fl3[y][x]['R']) >= z + 1:
                fnR = fl3[y][x]['R'][z]['function_name']
                cntLangs = cntLangs + 1
            if len(fl3[y][x]['Python']) >= z + 1:
                fnPython = fl3[y][x]['Python'][z]['function_name']
                cntLangs = cntLangs + 1 

## -- set null function names to 0 length string
            if fnSDTL is None : fnSDTL = ""
            if fnDefinition is None : fnDefinition = ""
            if fnSPSS is None : fnSPSS = ""
            if fnSAS is None : fnSAS = ""
            if fnStata is None : fnStata = ""
            if fnR is None : fnR = ""
            if fnPython is None : fnPython = ""

## -- print functions to terminal  ----
            if cntLangs>0 :
                print(f" {fnSDTL :30} {fnSPSS :10} {fnSAS :10} {fnStata :10} {fnR :10} {fnPython :10} " )

                ## -- write functions to CSV file --
                with open('FunctionLibraryList.csv', 'a', newline='') as csvfile:
                    outwrite = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
                    outwrite.writerow([y, fnSDTL, fnDefinition, fnSPSS, fnSAS, fnStata, fnR, fnPython ] )

## -- set function names to 0 length string for next loop
            fnSPSS = ""
            fnSAS = ""
            fnStata = ""
            fnR = ""
            fnPython = ""
         
         
         
         
